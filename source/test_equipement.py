# pylint: disable=missing-function-docstring
"""
Projet CyberAttack@IUT'O
SAÉ1.01 département Informatique IUT d'Orléans 2021-2022

Module test_equipement.py
Module de tests des fonctions d'implémentation des équipements
"""
import equipement


# Création d'équipements pour les tests
def equipement_1() -> dict:
    """
    Crée un premier equipement pour les fonctions de tests sur les équipements.
    """

    return equipement.creer_equipement(equipement.SERVEUR, 5)


def equipement_2() -> dict:
    """
    Crée un deuxième equipement pour les fonctions de tests sur les équipements.
    """

    return equipement.creer_equipement(equipement.SERVEUR, 7)


def equipement_3() -> dict:
    """
    Crée un troisième equipement pour les fonctions de tests sur les équipements.
    """

    return equipement.creer_equipement(equipement.ORDINATEUR, 3)


def equipement_4() -> dict:
    """
    Crée un quatrième equipement pour les fonctions de tests sur les équipements.
    """

    return equipement.creer_equipement(equipement.ORDINATEUR, 1)


def equipement_5() -> dict:
    """
    Crée un cinquième equipement pour les fonctions de tests sur les équipements.
    """

    return equipement.creer_equipement(equipement.SERVEUR, 0)


def equipement_6() -> dict:
    """
    Crée un sixième equipement pour les fonctions de tests sur les équipements.
    """

    return equipement.creer_equipement(equipement.ORDINATEUR, 0)


# Fonctions de tests
def test_get_resistance() -> None:
    serveur_1 = equipement_1()
    serveur_2 = equipement_2()
    ordi_1 = equipement_3()
    ordi_2 = equipement_4()

    assert equipement.get_resistance(serveur_1) == 5
    assert equipement.get_resistance(serveur_2) == 7
    assert equipement.get_resistance(ordi_1) == 3
    assert equipement.get_resistance(ordi_2) == 1


def test_attaque() -> None:
    serveur_1 = equipement_1()
    serveur_2 = equipement_2()
    ordi_1 = equipement_3()
    ordi_2 = equipement_4()

    equipement.attaque(serveur_1)
    assert equipement.get_resistance(serveur_1) == 4

    equipement.attaque(serveur_2)
    assert equipement.get_resistance(serveur_2) == 6

    equipement.attaque(ordi_1)
    assert equipement.get_resistance(ordi_1) == 2

    equipement.attaque(ordi_2)
    assert equipement.get_resistance(ordi_2) == 0


def test_est_detruit() -> None:
    serveur_1 = equipement_1()
    serveur_2 = equipement_5()
    ordi_1 = equipement_3()
    ordi_2 = equipement_6()

    assert not equipement.est_detruit(serveur_1)
    assert equipement.est_detruit(serveur_2)
    assert not equipement.est_detruit(ordi_1)
    assert equipement.est_detruit(ordi_2)


def test_get_type() -> None:
    serveur_1 = equipement_1()
    serveur_2 = equipement_2()
    ordi_1 = equipement_3()
    ordi_2 = equipement_4()

    assert equipement.get_type(serveur_1) == equipement.SERVEUR
    assert equipement.get_type(serveur_2) == equipement.SERVEUR
    assert equipement.get_type(ordi_1) == equipement.ORDINATEUR
    assert equipement.get_type(ordi_2) == equipement.ORDINATEUR


def test_set_resistance() -> None:
    serveur_1 = equipement_1()
    serveur_2 = equipement_2()
    ordi_1 = equipement_3()
    ordi_2 = equipement_4()

    equipement.set_resistance(serveur_1, 8)
    assert equipement.get_resistance(serveur_1) == 8

    equipement.set_resistance(serveur_2, 2)
    assert equipement.get_resistance(serveur_2) == 2

    equipement.set_resistance(ordi_1, 0)
    assert equipement.get_resistance(ordi_1) == 0

    equipement.set_resistance(ordi_2, 6)
    assert equipement.get_resistance(ordi_2) == 6
