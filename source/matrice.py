"""
Projet CyberAttack@IUT'O
SAÉ1.01 département Informatique IUT d'Orléans 2021-2022

Module matrice.py
Ce module contient des fonctions permettant de manipuler des matrices.
"""


def creer_matrice(nb_lignes: int, nb_colonnes: int, val_par_defaut: any = None) -> dict:
    """
    Crée une matrice contenant nb_lignes lignes et nb_colonnes colonnes avec
    pour valeur par défaut val_par_defaut.

    Args:
        nb_lignes (int): un entier strictement positif
        nb_colonnes (int): un entier strictement positif
        val_par_defaut (Any, optional): la valeur par défaut des éléments de la matrice.
        Defaults to None.

    Returns:
        dict: la matrice
    """

    matrice = {}

    for ligne in range(nb_lignes):
        for colonne in range(nb_colonnes):
            matrice[(ligne, colonne)] = val_par_defaut

    return matrice


def get_nb_lignes(matrice: dict) -> int:
    """
    Retourne le nombre de lignes de la matrice.

    Args:
        matrice (dict): une matrice

    Returns:
        int: le nombre de lignes de la matrice
    """

    # Les clés du dictionnaire sont sous la forme de tuple (ligne, colonne).
    # On récupère le dernier tuple pour être sûr d'avoir l'indice de la dernière
    # ligne, auquel on ajoute 1 pour avoir le nombre total de lignes.

    return list(matrice)[-1][0] + 1


def get_nb_colonnes(matrice: dict) -> int:
    """
    Retourne le nombre de colonnes de la matrice.

    Args:
        matrice (dict): une matrice

    Returns:
        int: le nombre de colonnes de la matrice
    """

    # Les clés du dictionnaire sont sous la forme de tuple (ligne, colonne).
    # On récupère le dernier tuple pour être sûr d'avoir l'indice de la dernière
    # ligne et donc d'avoir parcouru tous les indices de la matrice. Ensuite, on
    # récupère l'indice de la colonne auquel on ajoute un pour avoir le nombre
    # total de colonnes.

    return list(matrice)[-1][1] + 1


def get_val(matrice: dict, ligne: int, colonne: int) -> any:
    """
    Retourne la valeur en ligne, colonne de la matrice.

    Args:
        matrice (dict): une matrice
        lig (int): numéro de la ligne (en commençant par 0)
        col (int): numéro de la colonne (en commençant par 0)

    Returns:
        Any: la valeur en lig, col de la matrice
    """

    # On fait un traitement d'erreurs au cas où la position (ligne, colonne) ne soit
    # pas dans le dictionnaire. S'il y a erreur, on renvoie None.

    try:
        valeur = matrice.get((ligne, colonne))
    except KeyError:
        valeur = None

    return valeur


def set_val(matrice: dict, ligne: int, colonne: int, valeur: any) -> None:
    """
    Stocke la valeur valeur en ligne, colonne de la matrice.

    Args:
        matrice (dict): une matrice
        ligne (int): numéro de la ligne (en commençant par 0)
        colonne (int): numéro de la colonne (en commençant par 0)
        valeur (Any): la valeur à stocker
    """

    matrice[(ligne, colonne)] = valeur


def max_matrice(matrice: dict, interdits: set = None) -> tuple or None:
    """
    Retourne la liste des coordonnées des case contenant la valeur la plus grande de la matrice.
    Ces case ne doivent pas être parmis les interdits

    Args:
        matrice (dict): une matrice
        interdits (set): un ensemble de tuples (ligne, colonne) de case interdites. Defaults to None

    Returns:
        tuple: les coordonnées de case de valeur maximale dans la matrice (hors cases interdites).
        None si aucune valeur maximale trouvée.
    """

    valeur_max = 0
    indice_max = None
    nb_lignes = get_nb_lignes(matrice)
    nb_colonnes = get_nb_colonnes(matrice)

    for ligne in range(nb_lignes):
        for colonne in range(nb_colonnes):
            if (
                (interdits is not None and (ligne, colonne) not in interdits) or
                interdits is None
            ):
                valeur = get_val(matrice, ligne, colonne)
                
                if valeur > valeur_max:
                    valeur_max = valeur
                    indice_max = (ligne, colonne)

    return indice_max


DICO_DIR = {
    (-1, -1): "HD",
    (-1, 0): "HH",
    (-1, 1): "HD",
    (0, -1): "GG",
    (0, 1): "DD",
    (1, -1): "BG",
    (1, 0): "BB",
    (1, 1): "BD",
    (0, 0): "BB"
}


def direction_max_voisin(matrice: dict, ligne: int, colonne: int):
    """
    Retourne la liste des directions qui permette d'aller vers la case voisine de (ligne, colonne)
    la plus grande avec en plus la direction qui permet de se rapprocher du milieu de la matrice
    si ligne, colonne n'est pas le milieu de la matrice.

    Args:
        matrice (dict): une matrice
        ligne (int): le numéro de la ligne de la case considérée
        colonne (int): le numéro de la colonne de la case considérée

    Returns:
        str: deux lettres indiquant la direction DD -> droite , HD -> Haut Droite,
                                                 HH -> Haut, HG -> Haut gauche,
                                                 GG -> Gauche, BG -> Bas Gauche, BB -> Bas
    """
    ...
