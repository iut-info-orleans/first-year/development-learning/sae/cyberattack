# pylint: disable=missing-function-docstring
"""
Projet CyberAttack@IUT'O
SAÉ1.01 département Informatique IUT d'Orléans 2021-2022

Module test_case.py
Fonctions de tests du module case, qui gère les cases du plateau
"""
import case
import trojan
import protection
import test_protection
import test_equipement
import test_trojan


# Création de cases pour les fonctions de tests
def creer_case_1() -> dict:
    """
    Crée une première case pour les fonctions de tests.
    """

    protection_1 = test_protection.protection_1()
    serveur = test_equipement.equipement_1()
    trojants_presents = list(test_trojan.creer_quatre_trojans_1())
    trojans_entrants = list(test_trojan.creer_quatre_trojans_2())

    return case.creer_case(
        "B",
        protection_1,
        serveur,
        trojants_presents,
        trojans_entrants,
        True,
    )


def creer_case_2() -> dict:
    """
    Crée une deuxième case pour les fonctions de tests.
    """

    protection_2 = None
    serveur = test_equipement.equipement_2()
    trojants_presents = []
    trojans_entrants = []

    return case.creer_case(
        "G",
        protection_2,
        serveur,
        trojants_presents,
        trojans_entrants,
        True,
    )


def creer_case_3() -> dict:
    """
    Crée une troisième case pour les fonctions de tests.
    """

    protection_3 = test_protection.protection_2()
    serveur = None
    trojants_presents = []
    trojans_entrants = list(test_trojan.creer_quatre_trojans_4())

    return case.creer_case(
        "",
        protection_3,
        serveur,
        trojants_presents,
        trojans_entrants,
    )


def creer_case_4() -> dict:
    """
    Crée une quatrième case pour les fonctions de tests.
    """

    protection_4 = None
    serveur = None
    trojants_presents = list(test_trojan.creer_quatre_trojans_3())
    trojans_entrants = []

    return case.creer_case(
        "",
        protection_4,
        serveur,
        trojants_presents,
        trojans_entrants,
    )


# Fonctions de tests
def test_get_fleche() -> None:
    case_1 = creer_case_1()
    case_2 = creer_case_2()
    case_3 = creer_case_3()
    case_4 = creer_case_4()

    assert case.get_fleche(case_1) == "B"
    assert case.get_fleche(case_2) == "G"
    assert case.get_fleche(case_3) == ""
    assert case.get_fleche(case_4) == ""


def test_get_protection() -> None:
    case_1 = creer_case_1()
    case_2 = creer_case_2()
    case_3 = creer_case_3()
    case_4 = creer_case_4()

    assert case.get_protection(case_1) == test_protection.protection_1()
    assert case.get_protection(case_2) is None
    assert case.get_protection(case_3) == test_protection.protection_2()
    assert case.get_protection(case_4) is None


def test_get_serveur() -> None:
    case_1 = creer_case_1()
    case_2 = creer_case_2()
    case_3 = creer_case_3()
    case_4 = creer_case_4()

    assert case.get_serveur(case_1) == test_equipement.equipement_1()
    assert case.get_serveur(case_2) == test_equipement.equipement_2()
    assert case.get_serveur(case_3) is None
    assert case.get_serveur(case_4) is None


def test_get_trojans_presents() -> None:
    case_1 = creer_case_1()
    case_2 = creer_case_2()
    case_3 = creer_case_3()
    case_4 = creer_case_4()

    assert case.get_trojans_presents(case_1) == list(test_trojan.creer_quatre_trojans_1())
    assert case.get_trojans_presents(case_2) == []
    assert case.get_trojans_presents(case_3) == []
    assert case.get_trojans_presents(case_4) == list(test_trojan.creer_quatre_trojans_3())


def test_get_trojans_entrants() -> None:
    case_1 = creer_case_1()
    case_2 = creer_case_2()
    case_3 = creer_case_3()
    case_4 = creer_case_4()

    assert case.get_trojans_entrants(case_1) == list(test_trojan.creer_quatre_trojans_2())
    assert case.get_trojans_entrants(case_2) == []
    assert case.get_trojans_entrants(case_3) == list(test_trojan.creer_quatre_trojans_4())
    assert case.get_trojans_entrants(case_4) == []


def test_set_fleche() -> None:
    case_1 = creer_case_1()
    case_2 = creer_case_2()
    case_3 = creer_case_3()
    case_4 = creer_case_4()

    case.set_fleche(case_1, "D")
    assert case.get_fleche(case_1) == "D"

    case.set_fleche(case_2, "H")
    assert case.get_fleche(case_2) == "H"

    case.set_fleche(case_3, "G")
    assert case.get_fleche(case_3) == "G"

    case.set_fleche(case_4, "B")
    assert case.get_fleche(case_4) == "B"


def test_set_serveur() -> None:
    case_1 = creer_case_1()
    case_2 = creer_case_2()
    case_3 = creer_case_3()
    case_4 = creer_case_4()

    case.set_serveur(case_1, None)
    assert case.get_serveur(case_1) is None

    case.set_serveur(case_2, None)
    assert case.get_serveur(case_2) is None

    serveur_2 = test_equipement.equipement_2()
    case.set_serveur(case_3, serveur_2)
    assert case.get_serveur(case_3) == serveur_2

    serveur_1 = test_equipement.equipement_1()
    case.set_serveur(case_4, serveur_1)
    assert case.get_serveur(case_4) == serveur_1


def test_set_protection() -> None:
    case_1 = creer_case_1()
    case_2 = creer_case_2()
    case_3 = creer_case_3()
    case_4 = creer_case_4()

    case.set_protection(case_1, None)
    assert case.get_protection(case_1) is None

    protection_2 = test_protection.protection_2()
    case.set_protection(case_2, protection_2)
    assert case.get_protection(case_2) == protection_2

    case.set_protection(case_3, None)
    assert case.get_protection(case_3) is None

    protection_1 = test_protection.protection_1()
    case.set_protection(case_4, protection_1)
    assert case.get_protection(case_4) == protection_1


def test_set_les_trojans() -> None:
    case_1 = creer_case_1()
    case_2 = creer_case_2()
    case_3 = creer_case_3()
    case_4 = creer_case_4()

    trojans_1 = list(test_trojan.creer_quatre_trojans_1())
    trojans_2 = list(test_trojan.creer_quatre_trojans_2())
    trojans_3 = list(test_trojan.creer_quatre_trojans_3())
    trojans_4 = list(test_trojan.creer_quatre_trojans_4())

    case.set_les_trojans(case_1, trojans_4, [])
    assert case.get_trojans_presents(case_1) == trojans_4
    assert case.get_trojans_entrants(case_1) == []

    case.set_les_trojans(case_2, trojans_1, trojans_4)
    assert case.get_trojans_presents(case_2) == trojans_1
    assert case.get_trojans_entrants(case_2) == trojans_4

    case.set_les_trojans(case_3, trojans_3, trojans_2)
    assert case.get_trojans_presents(case_3) == trojans_3
    assert case.get_trojans_entrants(case_3) == trojans_2

    case.set_les_trojans(case_4, [], trojans_1)
    assert case.get_trojans_presents(case_4) == []
    assert case.get_trojans_entrants(case_4) == trojans_1


def test_ajouter_trojan() -> None:
    case_1 = creer_case_1()
    case_2 = creer_case_2()
    case_3 = creer_case_3()
    case_4 = creer_case_4()

    (trojan_1, trojan_2, trojan_3, trojan_4) = test_trojan.creer_quatre_trojans_5()

    trojans_case_1 = case.get_trojans_entrants(case_1)
    case.ajouter_trojan(case_1, trojan_1)
    assert trojan_1 in trojans_case_1

    trojans_case_2 = case.get_trojans_entrants(case_2)
    case.ajouter_trojan(case_2, trojan_2)
    assert trojan_2 in trojans_case_2

    trojans_case_3 = case.get_trojans_entrants(case_3)
    case.ajouter_trojan(case_3, trojan_3)
    assert trojan_3 in trojans_case_3

    trojans_case_4 = case.get_trojans_entrants(case_4)
    case.ajouter_trojan(case_4, trojan_4)
    assert trojan_4 in trojans_case_4


def test_mettre_a_jour_case() -> None:
    case_1 = creer_case_1()
    case_2 = creer_case_2()
    case_3 = creer_case_3()
    case_4 = creer_case_4()

    assert case.mettre_a_jour_case(case_1) == {
        1: 1,
        4: 3,
    }
    for trojan_present in case.get_trojans_presents(case_1):
        assert trojan.get_direction(trojan_present) == "B"
    assert case.get_protection(case_1) is None

    assert not case.mettre_a_jour_case(case_2)
    for trojan_present in case.get_trojans_presents(case_2):
        assert trojan.get_direction(trojan_present) == "G"
    assert case.get_protection(case_2) is None

    assert case.mettre_a_jour_case(case_3) == {
        1: 4,
    }
    protection_3 = case.get_protection(case_3)
    assert (
        case.get_protection(case_3) is not None and
        protection.get_resistance(protection_3) == 1
    )

    assert not case.mettre_a_jour_case(case_4)
    assert case.get_protection(case_4) is None


def test_contient_avatar() -> None:
    case_1 = creer_case_1()
    case_2 = creer_case_2()
    case_3 = creer_case_3()
    case_4 = creer_case_4()

    assert case.contient_avatar(case_1)
    assert case.contient_avatar(case_2)
    assert not case.contient_avatar(case_3)
    assert not case.contient_avatar(case_4)


def test_poser_avatar() -> None:
    case_1 = creer_case_1()
    case_2 = creer_case_2()
    case_3 = creer_case_3()
    case_4 = creer_case_4()

    assert case.poser_avatar(case_1) == {
        1: 1,
        2: 1,
        3: 1,
        4: 1,
    }
    assert case.contient_avatar(case_1)
    assert case.get_trojans_presents(case_1) == []

    assert not case.poser_avatar(case_2)
    assert case.contient_avatar(case_2)
    assert case.get_trojans_presents(case_2) == []

    assert not case.poser_avatar(case_3)
    assert case.contient_avatar(case_3)
    assert case.get_trojans_presents(case_3) == []

    assert case.poser_avatar(case_4) == {
        1: 2,
        3: 2,
    }
    assert case.contient_avatar(case_4)
    assert case.get_trojans_presents(case_4) == []


def test_enlever_avatar() -> None:
    case_1 = creer_case_1()
    case_2 = creer_case_2()
    case_3 = creer_case_3()
    case_4 = creer_case_4()

    case.enlever_avatar(case_1)
    assert not case.contient_avatar(case_1)

    case.enlever_avatar(case_2)
    assert not case.contient_avatar(case_2)

    case.enlever_avatar(case_3)
    assert not case.contient_avatar(case_3)

    case.enlever_avatar(case_4)
    assert not case.contient_avatar(case_4)


def test_reinit_trojans_entrants() -> None:
    case_1 = creer_case_1()
    case_2 = creer_case_2()
    case_3 = creer_case_3()
    case_4 = creer_case_4()

    case.reinit_trojans_entrants(case_1)
    assert case.get_trojans_entrants(case_1) == []

    case.reinit_trojans_entrants(case_2)
    assert case.get_trojans_entrants(case_2) == []

    case.reinit_trojans_entrants(case_3)
    assert case.get_trojans_entrants(case_3) == []

    case.reinit_trojans_entrants(case_4)
    assert case.get_trojans_entrants(case_4) == []
