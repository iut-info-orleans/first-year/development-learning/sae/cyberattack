"""
Projet CyberAttack@IUT'O
SAÉ1.01 département Informatique IUT d'Orléans 2021-2022

Module trojan.py
Ce module gère les chevaux de Troie appelés Trojan
"""


def creer_trojan(createur: int, type_t: int, direction: str) -> dict:
    """
    Crée un nouveau trojan.

    Args:
        createur (int): identifiant du créateur
        type_t (int): le type du trojan (un nombre entre 0 et 4)
        direction (str): une des directions 'H' 'B' 'G' ou 'D

    Returns:
        dict: un trojan
    """

    return {
        "createur": createur,
        "type": type_t,
        "direction": direction,
    }


def get_createur(trojan: dict) -> int:
    """
    Retourne l'identifiant du créateur du trojan.

    Args:
        trojan (dict): un trojan

    Returns:
        int: l'identifiant du créateur
    """

    return trojan.get("createur")


def get_type(trojan: dict) -> int:
    """
    Retourne le type du trojan.

    Args:
        trojan (dict): un trojan

    Returns:
        int: le  type du trojan (un nombre entre 0 et 4)
    """

    return trojan.get("type")


def get_direction(trojan: dict) -> str:
    """
    Retourne la direction dans laquelle le trojan se dirige.

    Args:
        trojan (dict): un trojan

    Returns:
        str: la direction du trojan
    """

    return trojan.get("direction")


def set_createur(trojan: dict, createur: int) -> None:
    """
    Change le créateur d'un trojan.

    Args:
        trojan (dict): un trojan
        createur (int): l'identifiant du nouveau créateur
    """

    trojan["createur"] = createur


def set_direction(trojan: dict, direction: str) -> None:
    """
    Change le créateur d'un trojan.

    Args:
        trojan (dict): un trojan
        direction (str): une des directions 'H' 'B' 'G' ou 'D
    """

    trojan["direction"] = direction


def set_type(trojan: dict, type_t: int) -> None:
    """
    Positionne le type du trojan.

    Args:
        trojan (dict): un trojan
        type_t (int): un nombre entre 0 et 4
    """

    trojan["type"] = type_t


def inverser_direction(trojan: dict) -> None:
    """
    Permet d'inverser la direction d'un trojan suivant le principe
    D -> G, G -> D, H -> B et B -> H.

    Args:
        trojan (dict): un trojan
    """

    nouvelles_directions = {
        "D": "G",
        "G": "D",
        "H": "B",
        "B": "H",
    }
    direction_actuelle = get_direction(trojan)

    set_direction(trojan, nouvelles_directions.get(direction_actuelle))


def changer_direction_angle_bdhg(trojan: dict) -> None:
    """
    Permet de faire rebondir un trojan de 45° sur un mur allant de bas droit vers haut gauche.
    D -> B, G -> H, H -> D et B -> G

    Args:
        trojan  (dict): un trojan
    """

    nouvelles_directions = {
        "D": "B",
        "G": "H",
        "H": "D",
        "B": "G",
    }
    direction_actuelle = get_direction(trojan)

    set_direction(trojan, nouvelles_directions.get(direction_actuelle))


def changer_direction_angle_bghd(trojan: dict) -> None:
    """
    Permet de faire rebondir un trojan de 45° sur un mur allant de bas gauche vers haut droit.
    D -> H, G -> B, H -> G et B -> D

    Args:
        trojan  (dict): un trojan
    """

    nouvelles_directions = {
        "D": "H",
        "G": "B",
        "H": "G",
        "B": "D",
    }
    direction_actuelle = get_direction(trojan)

    set_direction(trojan, nouvelles_directions.get(direction_actuelle))
