# pylint: disable=missing-function-docstring
"""
Projet CyberAttack@IUT'O
SAÉ1.01 département Informatique IUT d'Orléans 2021-2022

Module test_matrice.py
Ce module permet de tester les fonctions du module des matrices.
"""
import matrice


# Création de matrices pour les fonctions de tests.
def creer_matrice_1() -> dict:
    """
    Crée une première matrice pour les fonctions de tests.
    """

    matrice_1 = matrice.creer_matrice(4, 4)

    matrice.set_val(matrice_1, 0, 0, 3)
    matrice.set_val(matrice_1, 0, 1, 8)
    matrice.set_val(matrice_1, 0, 2, 5)
    matrice.set_val(matrice_1, 0, 3, 1)
    matrice.set_val(matrice_1, 1, 0, 7)
    matrice.set_val(matrice_1, 1, 1, 0)
    matrice.set_val(matrice_1, 1, 2, 4)
    matrice.set_val(matrice_1, 1, 3, 9)
    matrice.set_val(matrice_1, 2, 0, 19)
    matrice.set_val(matrice_1, 2, 1, 2)
    matrice.set_val(matrice_1, 2, 2, 5)
    matrice.set_val(matrice_1, 2, 3, 3)
    matrice.set_val(matrice_1, 3, 0, 20)
    matrice.set_val(matrice_1, 3, 1, 26)
    matrice.set_val(matrice_1, 3, 2, 31)
    matrice.set_val(matrice_1, 3, 3, 13)

    return matrice_1


def creer_matrice_2() -> dict:
    """
    Crée une seconde matrice pour les fonctions de tests.
    """

    matrice_2 = matrice.creer_matrice(2, 2)

    matrice.set_val(matrice_2, 0, 0, 4)
    matrice.set_val(matrice_2, 0, 1, 3)
    matrice.set_val(matrice_2, 1, 0, 2)
    matrice.set_val(matrice_2, 1, 1, 1)

    return matrice_2


def creer_matrice_3() -> dict:
    """
    Créer une troisième matrice pour les fonctions de tests.
    """

    matrice_3 = matrice.creer_matrice(3, 3)

    matrice.set_val(matrice_3, 0, 0, 1)
    matrice.set_val(matrice_3, 0, 1, 3)
    matrice.set_val(matrice_3, 0, 2, 5)
    matrice.set_val(matrice_3, 1, 0, 7)
    matrice.set_val(matrice_3, 1, 1, 9)
    matrice.set_val(matrice_3, 1, 2, 11)
    matrice.set_val(matrice_3, 2, 0, 13)
    matrice.set_val(matrice_3, 2, 1, 15)
    matrice.set_val(matrice_3, 2, 2, 17)

    return matrice_3


def creer_matrice_4() -> dict:
    """
    Crée une quatrième matrice pour les fonctions de tests.
    """

    matrice_4 = matrice.creer_matrice(2, 2)

    matrice.set_val(matrice_4, 0, 0, 0)
    matrice.set_val(matrice_4, 0, 1, 4)
    matrice.set_val(matrice_4, 1, 0, 9)
    matrice.set_val(matrice_4, 1, 1, 3)

    return matrice_4


# Fonctions de tests
def test_get_nb_lignes() -> None:
    matrice_1 = creer_matrice_1()
    matrice_2 = creer_matrice_2()
    matrice_3 = creer_matrice_3()
    matrice_4 = creer_matrice_4()

    assert matrice.get_nb_lignes(matrice_1) == 4
    assert matrice.get_nb_lignes(matrice_2) == 2
    assert matrice.get_nb_lignes(matrice_3) == 3
    assert matrice.get_nb_lignes(matrice_4) == 2


def test_get_nb_colonnes() -> None:
    matrice_1 = creer_matrice_1()
    matrice_2 = creer_matrice_2()
    matrice_3 = creer_matrice_3()
    matrice_4 = creer_matrice_4()

    assert matrice.get_nb_colonnes(matrice_1) == 4
    assert matrice.get_nb_colonnes(matrice_2) == 2
    assert matrice.get_nb_colonnes(matrice_3) == 3
    assert matrice.get_nb_colonnes(matrice_4) == 2


def test_get_val() -> None:
    matrice_1 = creer_matrice_1()
    matrice_2 = creer_matrice_2()
    matrice_3 = creer_matrice_3()
    matrice_4 = creer_matrice_4()

    assert matrice.get_val(matrice_1, 2, 0) == 19
    assert matrice.get_val(matrice_2, 0, 1) == 3
    assert matrice.get_val(matrice_3, 2, 2) == 17
    assert matrice.get_val(matrice_4, 1, 0) == 9


def test_set_val() -> None:
    matrice_1 = creer_matrice_1()
    matrice_2 = creer_matrice_2()
    matrice_3 = creer_matrice_3()
    matrice_4 = creer_matrice_4()

    matrice.set_val(matrice_1, 1, 0, 6)
    assert matrice.get_val(matrice_1, 1, 0) == 6

    matrice.set_val(matrice_2, 0, 0, 3)
    assert matrice.get_val(matrice_2, 0, 0) == 3

    matrice.set_val(matrice_3, 2, 2, 15)
    assert matrice.get_val(matrice_3, 2, 2) == 15

    matrice.set_val(matrice_4, 0, 1, 8)
    assert matrice.get_val(matrice_4, 0, 1) == 8


def test_max_matrice() -> None:
    matrice_1 = creer_matrice_1()
    matrice_2 = creer_matrice_2()
    matrice_3 = creer_matrice_3()
    matrice_4 = creer_matrice_4()

    interdits_1 = {(2, 0), (3, 0), (3, 1), (3, 2)}
    interdits_2 = {(0, 0)}
    interdits_3 = {(2, 1), (2, 2)}

    assert matrice.max_matrice(matrice_1, interdits_1) == (3, 3)
    assert matrice.max_matrice(matrice_2, interdits_2) == (0, 1)
    assert matrice.max_matrice(matrice_3, interdits_3) == (2, 0)
    assert matrice.max_matrice(matrice_4) == (1, 0)
