"""
Projet CyberAttack@IUT'O
SAÉ1.01 département Informatique IUT d'Orléans 2021-2022

Module protection.py
Gère les protections que les joueurs peuvent utiliser
"""


DPO = 0
FIREWALL_BDHG = 1
FIREWALL_BGHD = 2
DONNEES_PERSONNELLES = 3
ANTIVIRUS = 4
PAS_DE_PROTECTION = 5

RESISTANCE = 2


def creer_protection(type_p: int, resistance: int = RESISTANCE) -> dict:
    """
    Crée une protection.

    Args:
        type_p (int): type de la protection
        resistance (int, optional): nombre d'attaques que peut supporter la protection

    Returns:
        dict: une protection
    """

    return {
        "type": type_p,
        "resistance": resistance,
    }


def get_type(protection: dict) -> int:
    """
    Retourne le type de la protection.

    Args:
        protection (dict): une protection

    Returns:
        int: le type de la protection
    """

    return protection.get("type")


def get_resistance(protection: dict) -> int:
    """
    Retourne la résistance de la protection.

    Args:
        protection (dict): une protection

    Returns:
        int: la resistance de la protection
    """

    return protection.get("resistance")


def enlever_resistance(protection: dict) -> int:
    """
    Enlève un point de résistance de la protection et retourne la resistance restante.

    Args:
        protection (dict): une protection

    Returns:
        int: la resistance restante
    """

    protection["resistance"] -= 1

    return get_resistance(protection)


def est_detruite(protection: dict) -> bool:
    """
    Renvoie True si la protection est détruite, False sinon.

    Args:
        protection (dict): une protection

    Returns:
        bool: True si la protection est detruite
    """

    return get_resistance(protection) == 0


def set_type(protection: dict, type_p: int) -> None:
    """
    Change le type d'une protection.

    Args:
        protection (dict): une protection
        type_p (int): le nouveau type de la protection
    """

    protection["type"] = type_p


def set_resistance(protection: dict, resistance: int) -> None:
    """
    Définit une résistance pour une protection.

    Args:
        protection (dict): une protection
        resistance (int): la valeur de la résistance
    """

    protection["resistance"] = resistance
