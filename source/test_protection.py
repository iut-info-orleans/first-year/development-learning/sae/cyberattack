# pylint: disable=missing-function-docstring
"""
Projet CyberAttack@IUT'O
SAÉ1.01 département Informatique IUT d'Orléans 2021-2022

Module test_protection.py
Teste les différentes fonctions d'implémentation des protections.
"""
import protection


# Création de protections pour réaliser les tests
def protection_1() -> dict:
    """
    Crée une première protection pour réaliser les tests sur les protection.
    """

    return protection.creer_protection(protection.ANTIVIRUS)


def protection_2() -> dict:
    """
    Crée une deuxième protection pour réaliser les tests sur les protection.
    """

    return protection.creer_protection(protection.FIREWALL_BDHG, 5)


def protection_3() -> dict:
    """
    Crée une troisième protection pour réaliser les tests sur les protection.
    """

    return protection.creer_protection(protection.DPO)


def protection_4() -> dict:
    """
    Crée une quatrième protection pour réaliser les tests sur les protection.
    """

    return protection.creer_protection(protection.DONNEES_PERSONNELLES, 8)


def protection_5() -> dict:
    """
    Crée une cinquième protection pour réaliser les tests sur les protection.
    """

    return protection.creer_protection(protection.FIREWALL_BGHD, 0)


def protection_6() -> dict:
    """
    Crée une sixième protection pour réaliser les tests sur les protection.
    """

    return protection.creer_protection(protection.PAS_DE_PROTECTION, 0)


# Fonctions de tests
def test_get_type() -> None:
    antivirus = protection_1()
    firewall_bdhg = protection_2()
    dpo = protection_3()
    donnees_persos = protection_4()

    assert protection.get_type(antivirus) == protection.ANTIVIRUS
    assert protection.get_type(firewall_bdhg) == protection.FIREWALL_BDHG
    assert protection.get_type(dpo) == protection.DPO
    assert protection.get_type(donnees_persos) == protection.DONNEES_PERSONNELLES


def test_get_resistance() -> None:
    antivirus = protection_1()
    firewall_bdhg = protection_2()
    dpo = protection_3()
    donnees_persos = protection_4()

    assert protection.get_resistance(antivirus) == protection.RESISTANCE
    assert protection.get_resistance(firewall_bdhg) == 5
    assert protection.get_resistance(dpo) == protection.RESISTANCE
    assert protection.get_resistance(donnees_persos) == 8


def test_enlever_resistance() -> None:
    antivirus = protection_1()
    firewall_bdhg = protection_2()
    dpo = protection_3()
    donnees_persos = protection_4()

    assert protection.enlever_resistance(antivirus) == protection.RESISTANCE - 1
    assert protection.enlever_resistance(firewall_bdhg) == 4
    assert protection.enlever_resistance(dpo) == protection.RESISTANCE - 1
    assert protection.enlever_resistance(donnees_persos) == 7


def test_est_detruite() -> None:
    antivirus = protection_1()
    firewall_bdhg = protection_2()
    firewall_bghd = protection_5()
    pas_de_protection = protection_6()

    assert not protection.est_detruite(antivirus)
    assert not protection.est_detruite(firewall_bdhg)
    assert protection.est_detruite(firewall_bghd)
    assert protection.est_detruite(pas_de_protection)


def test_set_type() -> None:
    antivirus = protection_1()
    firewall_bdhg = protection_2()
    firewall_bghd = protection_5()
    pas_de_protection = protection_6()

    protection.set_type(antivirus, protection.DPO)
    assert protection.get_type(antivirus) == protection.DPO

    protection.set_type(firewall_bdhg, protection.DONNEES_PERSONNELLES)
    assert protection.get_type(firewall_bdhg) == protection.DONNEES_PERSONNELLES

    protection.set_type(firewall_bghd, protection.PAS_DE_PROTECTION)
    assert protection.get_type(firewall_bghd) == protection.PAS_DE_PROTECTION

    protection.set_type(pas_de_protection, protection.ANTIVIRUS)
    assert protection.get_type(pas_de_protection) == protection.ANTIVIRUS


def test_set_resistance() -> None:
    antivirus = protection_1()
    firewall_bdhg = protection_2()
    dpo = protection_3()
    donnees_persos = protection_4()

    protection.set_resistance(antivirus, 4)
    assert protection.get_resistance(antivirus) == 4

    protection.set_resistance(firewall_bdhg, 0)
    assert protection.get_resistance(firewall_bdhg) == 0

    protection.set_resistance(dpo, 1)
    assert protection.get_resistance(dpo) == 1

    protection.set_resistance(donnees_persos, 6)
    assert protection.get_resistance(donnees_persos) == 6
