"""
Projet CyberAttack@IUT'O
SAÉ1.01 département Informatique IUT d'Orléans 2021-2022

Module equipement.py
Ce module gère les équipements ordinateurs et serveurs
"""


SERVEUR = 0
ORDINATEUR = 1


def creer_equipement(type_e: int, resistance: int) -> dict:
    """
    Crée un équipement avec resistance.

    Args:
        type_e (int): type de l'équipement
        resistance (int): le nombre d'attaques nécessaires pour détruire l'équipement

    Returns:
        dict: une structure représentant l'équipement
    """

    return {
        "type": type_e,
        "resistance": resistance,
    }


def attaque(equipement: dict) -> None:
    """
    Enlève une protection à l'équipement.

    Args:
        equipement (dict): un équipement
    """

    equipement["resistance"] -= 1


def get_resistance(equipement: dict) -> int:
    """
    Retourne la resistance de l'équipement.

    Args:
        equipement (dict): un équipement

    Returns:
        int: la resistance restante de l'équipement
    """

    return equipement.get("resistance")


def est_detruit(equipement: dict) -> bool:
    """
    Indique si l'équipement est détruit (n'a plus de resistance).

    Args:
        equipement (dict): un équipement

    Returns:
        bool: True si l'équipement n'a plus de résistance et False sinon
    """

    return get_resistance(equipement) == 0


def get_type(equipement: dict) -> int:
    """
    Retourne le type de l'équipement.

    Args:
        equipement (dict): un équipement

    Returns:
        int: le type de l'équipement
    """

    return equipement.get("type")


def set_resistance(equipement: dict, resistance: int) -> None:
    """
    Positionne la résistance d'un équipement à une valeur donnée.

    Args:
        equipement (dict): un équipement
        resistance (int): la résistance restante de l'équipement
    """

    equipement["resistance"] = resistance
