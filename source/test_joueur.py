# pylint: disable=missing-function-docstring
"""
Projet CyberAttack@IUT'O
SAÉ1.01 département Informatique IUT d'Orléans 2021-2022

Module test_joueur.py
Module de tests sur les fonctions de gestion des joueurs.
"""
import joueur


# Création de joueurs pour les tests
def joueur_1() -> dict:
    """
    Crée un premier joueur pour les fonctions de tests.
    """

    return joueur.creer_joueur(1, "Emma")


def joueur_2() -> dict:
    """
    Crée un deuxième joueur pour les fonctions de tests.
    """

    return joueur.creer_joueur(2, "Damien", 2)


def joueur_3() -> dict:
    """
    Crée un troisième joueur pour les fonctions de tests.
    """

    return joueur.creer_joueur(3, "Julien", 1)


def joueur_4() -> dict:
    """
    Crée un quatrième joueur pour les fonctions de tests.
    """

    return joueur.creer_joueur(4, "Juliette", 3)


# Fonctions de tests
def test_get_id() -> None:
    emma = joueur_1()
    damien = joueur_2()
    julien = joueur_3()
    juliette = joueur_4()

    assert joueur.get_id(emma) == 1
    assert joueur.get_id(damien) == 2
    assert joueur.get_id(julien) == 3
    assert joueur.get_id(juliette) == 4


def test_get_nom() -> None:
    emma = joueur_1()
    damien = joueur_2()
    julien = joueur_3()
    juliette = joueur_4()

    assert joueur.get_nom(emma) == "Emma"
    assert joueur.get_nom(damien) == "Damien"
    assert joueur.get_nom(julien) == "Julien"
    assert joueur.get_nom(juliette) == "Juliette"


def test_get_points() -> None:
    emma = joueur_1()
    damien = joueur_2()
    julien = joueur_3()
    juliette = joueur_4()

    assert joueur.get_points(emma) == 0
    assert joueur.get_points(damien) == 2
    assert joueur.get_points(julien) == 1
    assert joueur.get_points(juliette) == 3


def test_ajouter_points() -> None:
    emma = joueur_1()
    damien = joueur_2()
    julien = joueur_3()
    juliette = joueur_4()

    assert joueur.ajouter_points(emma, 3) == 3
    assert joueur.ajouter_points(damien, 0) == 2
    assert joueur.ajouter_points(julien, 1) == 2
    assert joueur.ajouter_points(juliette, 2) == 5


def test_id_joueur_droite() -> None:
    emma = joueur_1()
    damien = joueur_2()
    julien = joueur_3()
    juliette = joueur_4()

    assert joueur.id_joueur_droite(emma) == joueur.get_id(damien)
    assert joueur.id_joueur_droite(damien) == joueur.get_id(julien)
    assert joueur.id_joueur_droite(julien) == joueur.get_id(juliette)
    assert joueur.id_joueur_droite(juliette) == joueur.get_id(emma)


def test_id_joueur_gauche() -> None:
    emma = joueur_1()
    damien = joueur_2()
    julien = joueur_3()
    juliette = joueur_4()

    assert joueur.id_joueur_gauche(emma) == joueur.get_id(juliette)
    assert joueur.id_joueur_gauche(damien) == joueur.get_id(emma)
    assert joueur.id_joueur_gauche(julien) == joueur.get_id(damien)
    assert joueur.id_joueur_gauche(juliette) == joueur.get_id(julien)


def test_id_joueur_haut() -> None:
    emma = joueur_1()
    damien = joueur_2()
    julien = joueur_3()
    juliette = joueur_4()

    assert joueur.id_joueur_haut(emma) == joueur.get_id(julien)
    assert joueur.id_joueur_haut(damien) == joueur.get_id(juliette)
    assert joueur.id_joueur_haut(julien) == joueur.get_id(emma)
    assert joueur.id_joueur_haut(juliette) == joueur.get_id(damien)
