"""
Projet CyberAttack@IUT'O
SAÉ1.01 département Informatique IUT d'Orléans 2021-2022

Module joueur.py
Module de gestion des joueurs
"""


def creer_joueur(id_joueur: int, nom_joueur: str, nb_points: int = 0) -> dict:
    """
    Crée un nouveau joueur.

    Args:
        id_joueur (int): l'identifiant du joueur (un entier de 1 à 4)
        nom_joueur (str): le nom du joueur
        nb_points (int, optional): le nombre de points du joueur. Defaults to 0.

    Returns:
        joueur (dict): le joueur
    """

    return {
        "id": id_joueur,
        "nom": nom_joueur,
        "nb_points": nb_points,
    }


def get_id(joueur: dict) -> int:
    """
    Retourne l'identifiant du joueur.

    Args:
        joueur (dict): un joueur

    Returns:
        int: l'identifiant du joueur
    """

    return joueur.get("id")


def get_nom(joueur: dict) -> str:
    """
    Retourne le nom du joueur.

    Args:
        joueur (dict): un joueur

    Returns:
        str: nom du joueur
    """

    return joueur.get("nom")


def get_points(joueur: dict) -> int:
    """
    Retourne le nombre de points du joueur.

    Args:
        joueur (dict): un joueur

    Returns:
        int: le nombre de points du joueur
    """

    return joueur.get("nb_points")


def ajouter_points(joueur: dict, points: int) -> int:
    """
    Ajoute des points au joueur.

    Args:
        joueur (dict): un joueur
        points (int): le nombre de points à ajouter

    Returns:
        int: le nombre de points du joueur
    """

    joueur["nb_points"] += points

    return get_points(joueur)


def id_joueur_droite(joueur: dict) -> int:
    """
    Retourne l'identifiant du joueur à droite d'un joueur.

    Args:
        joueur (dict): un joueur

    Returns:
        int: l'identifiant du joueur de droite
    """

    identifiant = get_id(joueur) + 1

    if identifiant > 4:
        return 1

    return identifiant


def id_joueur_gauche(joueur: dict) -> int:
    """
    Retourne l'identifiant du joueur à gauche d'un joueur.

    Args:
        joueur (dict): un joueur

    Returns:
        int: l'identifiant du joueur de gauche
    """

    identifiant = get_id(joueur) - 1

    if identifiant < 1:
        return 4

    return identifiant


def id_joueur_haut(joueur: dict) -> int:
    """
    Retourne l'identifiant du joueur au dessus d'un joueur.

    Args:
        joueur (dict): un joueur

    Returns:
        int: l'identifiant du joueur du haut
    """

    identifiant = get_id(joueur)

    if identifiant <= 2:
        return identifiant + 2

    return identifiant - 2
