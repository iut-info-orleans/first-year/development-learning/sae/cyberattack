"""
Projet CyberAttack@IUT'O
SAÉ1.01 département Informatique IUT d'Orléans 2021-2022

Module case.py
Ce module gère les cases du plateau
"""
import trojan
import protection


def creer_case(
    fleche: str,
    la_protection: dict or None,
    serveur: dict or None,
    liste_trojans_presents: list,
    liste_trojans_entrants: list,
    avatar: bool = False,
) -> dict:
    """
    Crée une case du plateau.

    Args:
        fleche (str): une des quatre directions 'H' 'B' 'G' 'D' ou '' si pas de flèche sur la case
        la_protection (dict or None): l'objet de protection posé sur la case (None si pas d'objet)
        serveur (dict or None): le serveur posé sur la case (None si pas de serveur)
        liste_trojans_presents (list): la liste des trojans présents sur la case
        liste_trojans_entrants (list): la liste des trojans qui vont arriver sur la case
        avatar (bool, optional): l'identifiant du joueur possédant l'avatar (None si pas d'avatar)

    Returns:
        dict: la représentation d'une case
    """

    return {
        "fleche": fleche,
        "protection": la_protection,
        "serveur": serveur,
        "trojans_presents": liste_trojans_presents,
        "trojans_entrants": liste_trojans_entrants,
        "avatar": avatar,
    }


def get_fleche(case: dict) -> str:
    """
    Retourne la direction de la flèche de la case.

    Args:
        case (dict): une case

    Returns:
        str: la direction 'H', 'B', 'G', 'D' ou '' si pas de flèche sur la case
    """

    return case.get("fleche")


def get_protection(case: dict) -> dict or None:
    """
    Retourne l'objet de protection qui se trouve sur la case.

    Args:
        case (dict): une case

    Returns:
        dict or None: l'objet de protection présent sur la case (None si pas d'objet)
    """

    return case.get("protection")


def get_serveur(case: dict) -> dict or None:
    """
    Retourne le serveur qui se trouve sur la case.

    Args:
        case (dict): une case

    Returns:
        dict or None: le serveur présent sur la case (None si pas d'objet)
    """

    return case.get("serveur")


def get_trojans_presents(case: dict) -> list:
    """
    Retourne la liste des trojans présents sur la case.

    Args:
        case (dict): une case

    Returns:
        list: la liste des trojans présents sur la case
    """

    return case.get("trojans_presents")


def get_trojans_entrants(case: dict) -> list:
    """
    Retourne la liste des trojans qui vont arriver sur la case.

    Args:
        case (dict): une case

    Returns:
        list: la liste des trojans qui vont arriver sur la case
    """

    return case.get("trojans_entrants")


def set_fleche(case: dict, direction: str) -> None:
    """
    Affecte une direction à la case.

    Args:
        case (dict): une case
        direction (dict): 'H', 'B', 'G', 'D' ou '' si pas de flèche sur la case
    """

    case["fleche"] = direction


def set_serveur(case: dict, serveur: dict or None) -> None:
    """
    Affecte un serveur à la case.

    Args:
        case (dict): une case
        serveur (dict or None): le serveur
    """

    case["serveur"] = serveur


def set_protection(case: dict, la_protection: dict or None):
    """
    Affecte une protection à la case.

    Args:
        case (dict): une case
        la_protection (dict): la protection
    """

    case["protection"] = la_protection


def set_les_trojans(case: dict, trojans_presents: list, trojans_entrants: list) -> None:
    """
    Fixe la liste des trojans présents et les trojans arrivant sur la case.

    Args:
        case (dict): une case
        trojans_presents (list): une liste de trojans
        trojans_entrants ([type]): une liste de trojans
    """

    case["trojans_presents"] = trojans_presents
    case["trojans_entrants"] = trojans_entrants


def ajouter_trojan(case: dict, un_trojan: dict) -> None:
    """
    Ajoute un nouveau trojan arrivant à une case.

    Args:
        case (dict): la case
        un_trojan (dict): le trojan à ajouter
    """

    case.get("trojans_entrants").append(un_trojan)


def dico_freq_trojans(liste_de_trojans: list) -> dict:
    """
    Génère un dictionnaire de fréquences de trojans par joueur.

    Args:
        liste_de_trojans (list): une liste de dictionnaires, contenant les trojans

    Returns:
        dict: le dictionnaire de fréquences des trojans selon les joueurs
    """

    frequence_trojans = {}

    for un_trojan in liste_de_trojans:
        createur = trojan.get_createur(un_trojan)

        if createur in frequence_trojans:
            frequence_trojans[createur] += 1
        else:
            frequence_trojans[createur] = 1

    return frequence_trojans


def mettre_a_jour_case(case: dict) -> dict:
    """
    Met les trojans arrivants comme présents et réinitialise les trojans arrivants.
    Change la direction du trojan si nécessaire (si la case comporte une flèche).

    La fonction retourne un dictionnaire qui indique pour chaque numéro de joueur
    le nombre de trojans qui vient d'arriver sur la case.

    La fonction enlève une resistance à protection qui se trouve sur elle et la détruit si
    la protection est arrivée à 0.

    Args:
        case (dict): la case

    Returns:
        dict: un dictionnaire dont les clés sont les numéros de joueur et les valeurs
              le nombre de trojans arrivés sur la case pour ce joueur
    """

    # Mise à jour des trojans
    trojans_entrants = get_trojans_entrants(case)

    set_les_trojans(case, trojans_entrants, [])

    trojans_presents = get_trojans_presents(case)
    protection_case = get_protection(case)

    for trojan_present in trojans_presents:
        # Changement de direction des trojans
        direction = get_fleche(case)

        if direction != "":
            trojan.set_direction(trojan_present, direction)

        # Gestion de la protection
        if protection_case is not None:
            protection.enlever_resistance(protection_case)

            if protection.est_detruite(protection_case):
                set_protection(case, None)

    # Dictionnaire de fréquence des trojans
    frequence_trojans = dico_freq_trojans(trojans_presents)

    return frequence_trojans


def poser_avatar(case: dict) -> dict:
    """
    Pose l'avatar sur cette case et élimine les trojans présents sur la case.
    La fonction indique combien de trojans ont été éliminés.

    Args:
        case (dict): une case

    Returns:
        dict: un dictionnaire dont les clés sont les numéros de joueur et les valeurs le nombre
        de trojans éliminés pour ce joueur.
    """

    case["avatar"] = True
    trojans_presents = get_trojans_presents(case)
    trojants_entrants = get_trojans_entrants(case)
    frequence_trojans = dico_freq_trojans(trojans_presents)

    set_les_trojans(case, [], trojants_entrants)

    return frequence_trojans


def enlever_avatar(case: dict) -> None:
    """
    Enlève l'avatar de la case.

    Args:
        case (dict): une case
    """

    case["avatar"] = False


def contient_avatar(case: dict) -> bool:
    """
    Vérifie si l'avatar se trouve sur la case.

    Args:
        case (dict): une case

    Returns:
        bool: True si l'avatar est sur la case et False sinon[type]: [description]
    """

    return case.get("avatar")


def reinit_trojans_entrants(case: dict) -> None:
    """
    Réinitialise la liste des trojans entrants à la liste vide.

    Args:
        case (dict): une case
    """

    trojans_presents = get_trojans_presents(case)

    set_les_trojans(case, trojans_presents, [])
