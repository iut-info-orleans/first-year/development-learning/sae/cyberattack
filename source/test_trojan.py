# pylint: disable=missing-function-docstring
"""
Projet CyberAttack@IUT'O
SAÉ1.01 département Informatique IUT d'Orléans 2021-2022

Module test_trojan.py
Ce module teste l'API de gestion des chevaux de Troie appelés Trojan
"""
import trojan


# Création de trojans pour les tests
def creer_quatre_trojans_1() -> tuple:
    """
    Crée quatre trojans pour les fonctions de tests.
    """

    trojan_1 = trojan.creer_trojan(1, 2, "G")
    trojan_2 = trojan.creer_trojan(2, 0, "D")
    trojan_3 = trojan.creer_trojan(3, 4, "H")
    trojan_4 = trojan.creer_trojan(4, 1, "B")

    return (trojan_1, trojan_2, trojan_3, trojan_4)


# Création d'autres trojans pour les fonctions de tests du module case
def creer_quatre_trojans_2() -> tuple:
    """
    Crée quatre trojans pour les fonctions de tests.
    """

    trojan_1 = trojan.creer_trojan(1, 1, "D")
    trojan_2 = trojan.creer_trojan(4, 4, "G")
    trojan_3 = trojan.creer_trojan(4, 3, "B")
    trojan_4 = trojan.creer_trojan(4, 0, "H")

    return (trojan_1, trojan_2, trojan_3, trojan_4)


def creer_quatre_trojans_3() -> tuple:
    """
    Crée quatre trojans pour les fonctions de tests.
    """

    trojan_1 = trojan.creer_trojan(1, 0, "H")
    trojan_2 = trojan.creer_trojan(1, 2, "B")
    trojan_3 = trojan.creer_trojan(3, 1, "G")
    trojan_4 = trojan.creer_trojan(3, 4, "D")

    return (trojan_1, trojan_2, trojan_3, trojan_4)


def creer_quatre_trojans_4() -> tuple:
    """
    Crée quatre trojans pour les fonctions de tests.
    """

    trojan_1 = trojan.creer_trojan(1, 4, "D")
    trojan_2 = trojan.creer_trojan(1, 1, "G")
    trojan_3 = trojan.creer_trojan(1, 0, "B")
    trojan_4 = trojan.creer_trojan(1, 3, "H")

    return (trojan_1, trojan_2, trojan_3, trojan_4)


def creer_quatre_trojans_5() -> tuple:
    """
    Crée quatre trojans pour les fonctions de tests.
    """

    trojan_1 = trojan.creer_trojan(1, 3, "H")
    trojan_2 = trojan.creer_trojan(2, 1, "G")
    trojan_3 = trojan.creer_trojan(3, 2, "D")
    trojan_4 = trojan.creer_trojan(4, 4, "B")

    return (trojan_1, trojan_2, trojan_3, trojan_4)


# Fonctions de tests
def test_get_createur() -> None:
    (trojan_1, trojan_2, trojan_3, trojan_4) = creer_quatre_trojans_1()

    assert trojan.get_createur(trojan_1) == 1
    assert trojan.get_createur(trojan_2) == 2
    assert trojan.get_createur(trojan_3) == 3
    assert trojan.get_createur(trojan_4) == 4


def test_get_type() -> None:
    (trojan_1, trojan_2, trojan_3, trojan_4) = creer_quatre_trojans_1()

    assert trojan.get_type(trojan_1) == 2
    assert trojan.get_type(trojan_2) == 0
    assert trojan.get_type(trojan_3) == 4
    assert trojan.get_type(trojan_4) == 1


def test_get_direction() -> None:
    (trojan_1, trojan_2, trojan_3, trojan_4) = creer_quatre_trojans_1()

    assert trojan.get_direction(trojan_1) == "G"
    assert trojan.get_direction(trojan_2) == "D"
    assert trojan.get_direction(trojan_3) == "H"
    assert trojan.get_direction(trojan_4) == "B"


def test_set_createur() -> None:
    (trojan_1, trojan_2, trojan_3, trojan_4) = creer_quatre_trojans_1()

    trojan.set_createur(trojan_1, 4)
    assert (
        trojan.get_createur(trojan_1) == 4
        and trojan.get_type(trojan_1) == 2
        and trojan.get_direction(trojan_1) == "G"
    )

    trojan.set_createur(trojan_2, 3)
    assert (
        trojan.get_createur(trojan_2) == 3
        and trojan.get_type(trojan_2) == 0
        and trojan.get_direction(trojan_2) == "D"
    )

    trojan.set_createur(trojan_3, 2)
    assert (
        trojan.get_createur(trojan_3) == 2
        and trojan.get_type(trojan_3) == 4
        and trojan.get_direction(trojan_3) == "H"
    )

    trojan.set_createur(trojan_4, 1)
    assert (
        trojan.get_createur(trojan_4) == 1
        and trojan.get_type(trojan_4) == 1
        and trojan.get_direction(trojan_4) == "B"
    )


def test_set_direction() -> None:
    (trojan_1, trojan_2, trojan_3, trojan_4) = creer_quatre_trojans_1()

    trojan.set_direction(trojan_1, "H")
    assert (
        trojan.get_createur(trojan_1) == 1
        and trojan.get_type(trojan_1) == 2
        and trojan.get_direction(trojan_1) == "H"
    )

    trojan.set_direction(trojan_2, "B")
    assert (
        trojan.get_createur(trojan_2) == 2
        and trojan.get_type(trojan_2) == 0
        and trojan.get_direction(trojan_2) == "B"
    )

    trojan.set_direction(trojan_3, "G")
    assert (
        trojan.get_createur(trojan_3) == 3
        and trojan.get_type(trojan_3) == 4
        and trojan.get_direction(trojan_3) == "G"
    )

    trojan.set_direction(trojan_4, "D")
    assert (
        trojan.get_createur(trojan_4) == 4
        and trojan.get_type(trojan_4) == 1
        and trojan.get_direction(trojan_4) == "D"
    )


def test_set_type() -> None:
    (trojan_1, trojan_2, trojan_3, trojan_4) = creer_quatre_trojans_1()

    trojan.set_type(trojan_1, 0)
    assert (
        trojan.get_createur(trojan_1) == 1
        and trojan.get_type(trojan_1) == 0
        and trojan.get_direction(trojan_1) == "G"
    )

    trojan.set_type(trojan_2, 3)
    assert (
        trojan.get_createur(trojan_2) == 2
        and trojan.get_type(trojan_2) == 3
        and trojan.get_direction(trojan_2) == "D"
    )

    trojan.set_type(trojan_3, 1)
    assert (
        trojan.get_createur(trojan_3) == 3
        and trojan.get_type(trojan_3) == 1
        and trojan.get_direction(trojan_3) == "H"
    )

    trojan.set_type(trojan_4, 6)
    assert (
        trojan.get_createur(trojan_4) == 4
        and trojan.get_type(trojan_4) == 6
        and trojan.get_direction(trojan_4) == "B"
    )


def test_inverser_direction() -> None:
    (trojan_1, trojan_2, trojan_3, trojan_4) = creer_quatre_trojans_1()

    trojan.inverser_direction(trojan_1)
    assert (
        trojan.get_createur(trojan_1) == 1
        and trojan.get_type(trojan_1) == 2
        and trojan.get_direction(trojan_1) == "D"
    )

    trojan.inverser_direction(trojan_2)
    assert (
        trojan.get_createur(trojan_2) == 2
        and trojan.get_type(trojan_2) == 0
        and trojan.get_direction(trojan_2) == "G"
    )

    trojan.inverser_direction(trojan_3)
    assert (
        trojan.get_createur(trojan_3) == 3
        and trojan.get_type(trojan_3) == 4
        and trojan.get_direction(trojan_3) == "B"
    )

    trojan.inverser_direction(trojan_4)
    assert (
        trojan.get_createur(trojan_4) == 4
        and trojan.get_type(trojan_4) == 1
        and trojan.get_direction(trojan_4) == "H"
    )


def test_changer_direction_angle_bdhg() -> None:
    (trojan_1, trojan_2, trojan_3, trojan_4) = creer_quatre_trojans_1()

    trojan.changer_direction_angle_bdhg(trojan_1)
    assert (
        trojan.get_createur(trojan_1) == 1
        and trojan.get_type(trojan_1) == 2
        and trojan.get_direction(trojan_1) == "H"
    )

    trojan.changer_direction_angle_bdhg(trojan_2)
    assert (
        trojan.get_createur(trojan_2) == 2
        and trojan.get_type(trojan_2) == 0
        and trojan.get_direction(trojan_2) == "B"
    )

    trojan.changer_direction_angle_bdhg(trojan_3)
    assert (
        trojan.get_createur(trojan_3) == 3
        and trojan.get_type(trojan_3) == 4
        and trojan.get_direction(trojan_3) == "D"
    )

    trojan.changer_direction_angle_bdhg(trojan_4)
    assert (
        trojan.get_createur(trojan_4) == 4
        and trojan.get_type(trojan_4) == 1
        and trojan.get_direction(trojan_4) == "G"
    )


def test_changer_direction_angle_bghd() -> None:
    (trojan_1, trojan_2, trojan_3, trojan_4) = creer_quatre_trojans_1()

    trojan.changer_direction_angle_bghd(trojan_1)
    assert (
        trojan.get_createur(trojan_1) == 1
        and trojan.get_type(trojan_1) == 2
        and trojan.get_direction(trojan_1) == "B"
    )

    trojan.changer_direction_angle_bghd(trojan_2)
    assert (
        trojan.get_createur(trojan_2) == 2
        and trojan.get_type(trojan_2) == 0
        and trojan.get_direction(trojan_2) == "H"
    )

    trojan.changer_direction_angle_bghd(trojan_3)
    assert (
        trojan.get_createur(trojan_3) == 3
        and trojan.get_type(trojan_3) == 4
        and trojan.get_direction(trojan_3) == "G"
    )

    trojan.changer_direction_angle_bghd(trojan_4)
    assert (
        trojan.get_createur(trojan_4) == 4
        and trojan.get_type(trojan_4) == 1
        and trojan.get_direction(trojan_4) == "D"
    )
